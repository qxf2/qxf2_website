"""
Generate the Qxf2 static pages based on the template, qhtml files and .conf
1. Read template
2. Read in conf
3. For each file/section in conf:
   * replace title
   * add 'active' link on the menu links
   * insert content from the right file in qhtml
"""

import os,sys
from ConfigParser import SafeConfigParser

BASEPATH = os.path.dirname(os.path.realpath(__file__))
OUTPUT_DIR = os.path.abspath(BASEPATH + os.sep + '..')
QHTML_DIR = os.path.abspath(BASEPATH + os.sep + 'qhtml')
CONF_FILE = os.path.abspath(BASEPATH + os.sep + '.conf')
TEMPLATE_FILE = 'template.html'
#--From conf options
TITLE = "title"
ACTIVE = "active"
OUTPUT_PAGE = "output_page"
#--Marker to replace in template
CONTENT_MARKER = "<!--REPLACE THIS LINE OF THE TEMPLATE-->"

def read_template(filename):
    "Read the template and return a list of lines"
    print 'Template file being read: ', filename
    fp = open(filename,'r')
    lines = fp.readlines()
    fp.close()

    return lines


def get_conf_obj(filename):
    "Return a config parser object by reading the conf file"
    print 'Reading the conf file: ',filename
    parser = SafeConfigParser()
    parser.read(filename)

    return parser


def replace_text(lines,str_to_look_for,str_to_replace):
    "Replace strings in a given list of strings"
    new_text = []
    for line in lines:
        line = line.replace(str_to_look_for, str_to_replace)
        new_text.append(line)

    return new_text


def replace_title(lines,title):
    "Replace the title in a given text"
    str_to_look_for = "<title>Qxf2 Services: Software testing services for startups</title>"
    str_to_replace = "<title>%s</title>"%title
    new_text = replace_text(lines,str_to_look_for,str_to_replace)

    return new_text


def add_active_link(lines,active_line):
    "Add the active class to the right td"
    #UPDATE: We are not showing the active link anymore
    """
    str_to_look_for = active_line
    str_to_replace = str_to_look_for.replace('col-md-2','active col-md-2')
    new_text = replace_text(lines,str_to_look_for,str_to_replace)    
    return new_text
    """
    return lines


def insert_content(lines,page):
    "Insert content from the qhtml page"
    filename = os.path.join(QHTML_DIR,page)
    new_text = []
    if os.path.exists(filename):
        fp = open(filename,'r')
        content = fp.readlines()
        fp.close()
        for line in lines:
            if CONTENT_MARKER in line:
                for content_line in content:
                    new_text.append(content_line)
            else:
                new_text.append(line)

    return new_text


def write_page(lines,page):
    "Write out the lines into page"
    print 'Writing out the page: ',page
    output_file = os.path.join(OUTPUT_DIR,page)
    fp = open(output_file,'w')
    for line in lines:
        fp.write(line)
    fp.close()

    return


def generate_site():
    "Generate the static HTML site"
    template = read_template(os.path.join(QHTML_DIR,TEMPLATE_FILE))
    conf_obj = get_conf_obj(CONF_FILE)
    for page in conf_obj.sections():
        print 'Generating page: ',page
        title = ''
        active_line = ''
        output_page = ''

        for name,value in conf_obj.items(page):
            if name == TITLE:
                title = value
            if name == ACTIVE:
                active_line = value
            if name == OUTPUT_PAGE:
                output_page = value
        lines = replace_title(template,title)
        lines = insert_content(lines,output_page)
        write_page(lines,output_page)


#---START OF SCRIPT
if __name__=='__main__':
    generate_site()
